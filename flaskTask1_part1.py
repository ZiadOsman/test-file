import random
import datetime
from pymongo import MongoClient
import pprint
import urllib.parse
username = urllib.parse.quote_plus('Ziad')
password = urllib.parse.quote_plus('123')
client = MongoClient('mongodb://%s:%s@127.0.0.1' % (username, password))

actors_list = []
titles_list = []
with open("actors list.txt","r") as f:
    for line in f:
        temp = line[:len(line)-1] #the slicing is to remove the "\n" before appending
        actors_list.append(temp)

with open("titles list.txt","r") as f:
    for line in f:
        temp = line[:len(line)-1] #the slicing is to remove the "\n" before appending
        titles_list.append(temp)

#code from internet to get random date in a given year
def get_random_date(year):
    
    # try to get a date
    try:
        temp = datetime.datetime.strptime('{} {}'.format(random.randint(1, 366), year), '%j %Y')
        temp = temp
        return temp

    # if the value happens to be in the leap year range, try again
    except ValueError:
        get_random_date(year)

def create_dict():
    temp_actors_list = actors_list
    temp_actors = []
    actors_nb = random.randint(0,len(actors_list))
    for i in range(actors_nb): #pick random actors, number of actors is determined randomly
        random.shuffle(temp_actors_list) #shuffle to keep it random
        temp_actors.append(temp_actors_list.pop()) #pop to prevent actor name appearing twice
    temp_dict = {
        "title" : str(random.choice(titles_list)),
        "actors": temp_actors,
        "date": get_random_date(random.randint(1940,2020))
        
    }
    return(temp_dict)


db = client.flask_test1
posts = db.posts

def insert_dict():
    insert_dict = create_dict()
    counter = 0
    #this code finds all the elements with the same title, actors list and date
    #if there are no such elements (when counter = 0), we can add the element
    # .find() finds all elements fitting the criteria, so we dont face the "ordering problem" mentioned in the email
    
    for i in posts.find({"title" : insert_dict.get("title"),
    "actors" : insert_dict.get("actors"),
    "date": insert_dict.get("date")}):
        counter+=1
    if counter == 0:
        posts.insert_one(insert_dict).inserted_id
    else:
        print("this movie is already in the database")


while db.posts.count() <30:
    insert_dict()

#note: the datetime object has hours and minutes and seconds, which is unhelpful and useless information for movies
#to remove the hours and minutes and seconds we can turn the object from datetime.datetime to datetime.date
#the problem is that BSON knows how to deal with datetime.datetime objects but not datetime.date objects
#to overcome this i can either turn the datetime.date object to str or create a custom encoder, but seeing as you specified you wanted the type of the object to stay datetime, i didnt do that yet
#waiting for feedback