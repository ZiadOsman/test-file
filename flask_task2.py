from flask import Flask ,request,url_for,render_template
from flask_pymongo import PyMongo
import datetime
from datetime import datetime
from pymongo import MongoClient
import pprint
import urllib.parse

username = urllib.parse.quote_plus('Ziad')
password = urllib.parse.quote_plus('123')
client = MongoClient('mongodb://%s:%s@127.0.0.1' % (username, password))
db = client.flask_test1
posts = db.posts

app = Flask(__name__)
app.config["MONGO_URI"] = "mongodb://localhost:27017/flask_test1"
#apparently configuring from the same file is bad practice so we configure in a seperate one
#we can set different configuration values based on if we're running as production or dev
#this requires setting a class in the config file
#did not bother with that here 

mongo = PyMongo(app)

@app.route('/')
def print_users():
    all_users = mongo.db.posts.find()
    return render_template("index.html",all_users=all_users)

@app.route('/title/<title>') #after local host in the url enter / and then add the title you want
def take_title(title):
    movie = mongo.db.posts.find({"title":str(title)} )
    return render_template("title.html",movie=movie)

@app.route('/date/<fromData>/<to>')
def date(fromData,to):
    movies_in_timeline = []
    for i in mongo.db.posts.find():
        #i have the wrong format for my datetime so i cant check which is bigger. couldnt get the real format, there is no function to get it.
        if datetime.strptime(fromData,'%y/%m/%d %H:%M:%S.%f') < i.get("date") and i.get("date") < datetime.strptime(to,'%y/%m/%d %H:%M:%S.%f'):
            movies_in_timeline.append(i)
    return render_template("timeline.html",movies_in_timeline = movies_in_timeline)



@app.route('/actors/<actors>')
def actors(actors):
    movies = []
    actors_list = actors.split(",")
    for i in mongo.db.posts.find():
        for j in i.get("actors"):
            if j in actors_list:
                movies.append(i)
                break
    return render_template("actors.html",movies=movies)



#this last one is still a work in progress 
@app.route("/add/",methods=['POST'])

def add(title,actors,date):
    adding_form = request.form
    if adding_form.validate():
        title = request.form.get("title")
        actors = request.form.get("actors").split(",")
        date = request.form.get("date")
        movie = {"title":title,
        "actors":actors,
        "date":date}
        mongo.db.posts.insert(movie)
        # mongo.db.posts.add(movie)
        # mongo.db.session.commit()
    return render_template("add.html")