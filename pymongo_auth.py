from pymongo import MongoClient
import pprint
import random
import urllib.parse
username = urllib.parse.quote_plus('Ziad')
password = urllib.parse.quote_plus('123')
client = MongoClient('mongodb://%s:%s@127.0.0.1' % (username, password))


db = client.database_auth
posts = db.posts

fname = ['Emma',
'Olivia',
'Ava',
'Isabella',
'Sophia',
'Charlotte',
'Mia',
'Amelia',
'Harper',
'Evelyn',
'Abigail',
'Emily',
'Elizabeth']
lname = ['Mila',
'Ella',
'Avery',
'Sofia',
'Camila',
'Aria',
'Scarlett',
'Victoria',
'Madison',
'Luna',
'Grace',
'Chloe',
'Penelope',
'Layla',
'Riley']

faculty = ["msfea","fas"]
msfea = ["cce","ece","civil","architecture","mechanical"]
fas = ["cmps","maths","statistics","biology","chemistry","psychology","english"]
nationality = ["lebanese","french","irish","american","canadian","australian"]

def make_dict():
    f_nb = random.randint(0,12)
    l_nb = random.randint(0,14)
    fac_nb = random.randint(0,1)
    nation_nb = random.randint(0,5)
    temp_dict = {
    "fname" : str(fname[f_nb]),
    "lname" : str(lname[l_nb]),
    "id" : random.randint(201800000,201999999),
    "nationality":str(nationality[nation_nb])
    }
    if fac_nb ==0:
        index = random.randint(0,4)
        temp_dict["faculty"] = "msfea"
        temp_dict["major"] = str(msfea[index])
    else:
        index = random.randint(0,6)
        temp_dict["faculty"] = "fas"
        temp_dict["major"] = str(fas[index])
    posts.insert_one(temp_dict).inserted_id

#insert 68 random dictionaries into the database
for i in range(68):
    make_dict()


#find all students in FAS
for i in db.posts.find({'faculty' : "fas"}): 
    pprint.pprint(i) 
    print('\n')



#find all lebanese cmps students
for i in db.posts.find({ 
  "major": "cmps",
  "nationality": "lebanese"}):
    pprint.pprint(i) 
    print('\n')


#find all lebanese students and all american students
for i in db.posts.find({ 
  "$or": [
    {
      "nationality": "lebanese"
    },
    {
      "nationality": "american"}]}):
    pprint.pprint(i)
    print("\n")


#combine and with or: all students in msfea that are either australian or canadian
for i in db.posts.find({
  "$and": [
    {
      "$or": [
        {
          "nationality": "australian"
        },
        {
          "nationality": "canadian"
        }
      ]
    },
    {
      "faculty": "msfea"}]}):
    pprint.pprint(i)
    print("\n")

#students that are not australian and are not in FAS 
for i in db.posts.find({
  $and: [
    {
      "nationality": {
        $ne: "australian
      }
    },
    {
      "faculty": {
        $ne: "fas"
      }}]}):
    pprint.pprint(i)
    print("\n")  
