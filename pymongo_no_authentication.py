import pymongo
import datetime
import pprint
from pymongo import MongoClient
from bson.objectid import ObjectId
client = MongoClient('localhost', 27017)
db = client.test_database #name of database is test_database
collection = db.test_collection
#Collections and databases are created when the first document is inserted into them.
#meaning that, for now, we havent actually created a databse or a collection 

test_dict = {
    "fname" : "Ziad",
    "lname" : "Osman",
    "id" : 201902597,
    "date": datetime.datetime.utcnow()
    #no serialization issues for datetime, they are handled correctly without having to make a custom encoder

}
posts = db.posts   
post_id= posts.insert_one(test_dict).inserted_id
print(post_id)#get the id of the element "test_dict" in the db

collection1 = db.collection1 
post_id = collection1.insert_one(test_dict).inserted_id
print(post_id) #testing if id changes when we put "test_dict" in another collection

print(db.list_collection_names()) #prints two collection names: "posts" and "collection1"


pprint.pprint(posts.find_one()) #print first element in posts
pprint.pprint(collection1.find_one()) #print first element in collection1

pprint.pprint(collection1.find_one({"fname": "Mohamad"})) #returns None since there is no element if fname mohamad in collection1

pprint.pprint(collection1.find_one({"_id": str(post_id)})) #returns None because an Objectid is not the same as its string representation
#to fix this issue, lets use objectid from bson
pprint.pprint(collection1.find_one({"_id": ObjectId(post_id)})) #this successfully prints the element "test_dict" from its id
